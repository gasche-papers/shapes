(** {1 Implementations of strong reduction}

   This file implements several strong reduction strategies for an
   untyped lambda-calculus. It was summarized during our presentation

     An OCaml use case for strong call-by-need reduction
     Gabriel Scherer and Nathanaëlle Courant

   at the ML Workshop 2022.

   This code can be "tested" simply by running
     $ ocaml code.ml
   If a test fails, the module fails with Assert_failure.

   The code was originally an extracted, simplified version of an
   implementation of a strong call-by-need evaluator inside the OCaml
   compiler

     https://github.com/ocaml/ocaml/pull/10825


   This code involved since, independently of the in-compiler
   implementation. The two main changes were the following:

   1. The initial code made no attempt to guarantee freshness of
     binders, and could therefore produce wrong results in presence of
     shadowing in the input program. This comes from the fact that the
     OCaml compiler guarantees that all binders are unique (it follows
     the Barendregt convention), using a type Ident.t of identifiers
     whose creation includes fresh symbol generation.

     This understandably did not please the ML Workshop reviewers, so we
     changed the code to work correctly in presence of non-unique binders.
     The main change is to move from
       | Clos of env * var * t * nf
         (* arguments:
           - captured dynamic environmnt
           - bound variable
           - body (in the input term)
           - normal form of the body *)
     to
       | Clos of env * var * t * var * nf
         (* arguments:
           - captured dynamic environmnt
           - bound variable
             (in the input term, scopes over the body)
           - body (in the input term)
           - fresh bound variable
             (generated during evaluation, scopes over the normal form)
           - normal form of the body *)


   2. When we prepared this presentation we discovered a direct
     correspondence between our "final" evaluator (close to the one in
     the OCaml compiler) and a memoized version of an iterated weak
     reduction strategy. We present this as a last implementation.
*)

(** {2 Preparation/buildup code}

    Feel free to skip to the {2 Implementations} section of first read.
*)

(** {3 Type definitions shared by all implementations} *)

(** This Ident module mirrors the interface of the Ident module of the
   OCaml compiler, but defined in terms of Map from the standard
   library. This lets us write code that looks similar to the
   in-compiler implementation, with lighter dependencies. *)
module Ident = struct
  module Ident = struct
    type t = string
    let compare = String.compare
  end
  include Ident
  module IMap = Map.Make(Ident)
  type 'a tbl = 'a IMap.t
  let add = IMap.add
  let find_same = IMap.find
  let empty = IMap.empty
end

(* program identifiers *)
type var = Ident.t

type t = (* terms *)
  | Var of var
  | Abs of var * t
  | App of t * t

(* Most implementations used a notion of "open value"
   being either a normal form (they use different types for this)
   or a free variable bound in the evaluation context *)
type 'v open_value =
  | Val of 'v
  | Free of var
type 'v open_env = 'v open_value Ident.tbl


(** {3 Variable handling code} *)

(* Our freshening implementation assumes that user variables
   may not contain the symbol '#', we check this dynamically. *)
let check_user_var str =
  if String.contains str '#' then
    Printf.ksprintf failwith
      "Invalid user variable ('#' not allowed): %s" str

let fresh =
  let r = ref 0 in
  fun str ->
    check_user_var str;
    incr r;
    str ^ "#" ^ (string_of_int !r)

let rec drop_stamps : t -> t =
  let trim x =
    match String.index x '#' with
    | exception Not_found -> x
    | i -> String.sub x 0 i
  in
  function
  | App (t, u) -> App (drop_stamps t, drop_stamps u)
  | Abs (x, t) -> Abs (trim x, drop_stamps t)
  | Var x -> Var (trim x)


(** {3 Testing code}

   You can skip this on first read and come back if you really care
   about our testing strategy. *)

(* We will test strong-reduction implementations presented as
   modules satisfying the Eval signature. *)
module type Eval = sig
  type v
  type env = v open_env
  val normalize : env -> t -> t
end

let tests =
  let id = Abs("y", Var "y") in
  let let_ x def body = App(Abs(x, body), def) in
  [
    (* format: (free variables, input, expected output) *)
     [],
      Abs ("x", App(id, Var "x")),
      Abs ("x", Var "x");
    ["res1"],
      let_ "id" id (App (Var "id", Var "res1")),
      Var "res1";
    ["res2"],
      App(
        let_ "id" id (Var "id"),
        Var "res2"
      ),
      Var "res2";
    ["unused"; "res3"],
      App(
        let_ "r" (Var "res3") (Abs ("_", Var "r")),
        Var "unused"
      ),
      Var "res3";

    (* an example of potential freshening-and-closure issues
       provided by the ML Workshop reviewers *)
    [],
      App (Abs ("x", Abs ("z", App (Var "x", Var "z"))),
           Abs ("w", Abs ("y", App (Var "w", Var "y")))),
      Abs ("z", Abs ("y", App (Var "z", Var "y")));
  ]


(* build a dynamic environment out of user-provided free variables *)
let abstract_env vars =
  List.fold_right (fun x ->
    check_user_var x;
    Ident.add x (Free x)
  ) vars Ident.empty


module type Eval = sig
  type v
  type env = v open_env
  val normalize : env -> t -> t
end

let test (module Eval : Eval) =
  tests |> List.map (fun (vars, input, expected_output) ->
    let env = abstract_env vars in
    let output, to_compare =
      match Eval.normalize env input with
      | exception e -> Error e, Error e
      | v -> Ok v, Ok (drop_stamps v) in
    let status = (Ok expected_output = to_compare) in
    (status, input, expected_output, output)
  )

let check_result res =
  List.iter (fun (status, _, _, _) -> assert status) res

(** {2 Implementations} *)

(** {3 Very naive}

    A zeroth, "very naive" implementation of strong reduction. *)
module Strong_very_naive = struct
  type v = t
  and env = v open_env

  (* Remark on scoping.

     The function [eval env t] takes an dynamic environment,
     a substitution from a static environment [Gamma] to some other
     static environment [Delta]. We expect [t] to live in [Gamma], and
     the result normal form lives in [Delta].

     (Terminology: abstract-machine people use "environment" for
     bindings from variables to values -- those really represent
     "substitutions". Type-theory people use "environment" for the
     binding context of the term. Above we used 'static environments"
     for binding contexts and "dynamic environments" for
     substitutions -- which transport terms from a static environment
     to another. Sometimes we will just use "environment" and hope it will
     be clear from the, ahem, context.)

     At the toplevel [eval] will be called with an "identity
     environment" mapping some free variables to themselves, so there
     is no actual change of static environemnt, but inside the
     computations the environment changes with beta-reductions: when
     evaluating
       eval env (App(Abs(x, t), u))
     we get morally (in pseudo-syntax)
       eval (env, x := eval env u) t
     notice that at this point we have [x] in the input environment
     [Gamma] -- it may occur in the input [t] -- but not in the output
     environment [Delta] -- it may not occur in the result value.
  *)
  let rec eval env : t -> t = function
    | Var x ->
      begin match Ident.find_same x env with
      | Val v -> v
      | Free x -> Var x
      end
    | Abs (x, t) ->
      Abs (x,
       let env' = Ident.add x (Free x) env in
       eval env' t)
      (* Remark on scoping: the body of the input abstraction
         is in environment (Gamma, x), and the body of the output
         abstraction is in environment (Delta, x). *)
    | App (t, u) ->
      let f, arg = eval env t, eval env u in
      match f with
      | (Var _ | App _) as ne -> App (ne, arg)
      | Abs (x, body) ->
        (* The main reason why this implementation is very naive is here:
           it passes as an argument to [eval] the [body] of the *normal form*
           of the original argument [t], which may be exponentially larger
           than [t] itself (and this implementation performs no sharing).

           This corresponds to a breach of the "subterm property" of evaluators,
           that only ever duplicate in the control code (input argument of the evaluator,
           head positions in the abstract machine...) subterms of the *original* input.
        *)
        eval (Ident.add x (Val arg) env) body

  let normalize = eval
end

let test_result = test (module Strong_very_naive : Eval)
let () = check_result test_result


(** {2 Naive}

    A first, "naive" implementation of strong reduction.

    To preserve the "subterm property" mentioned in the very naive
    evaluation above, we move to a representation of closures
    that stores both:
    - the original body of the function in the input term
      (used for closures that are applied)
    - the normal form of the body
      (used for closures that are returned as results)

    Note that this cannot be represented by our input type [t],
    so we introduce a new type [nf] of "normal forms" returned
    by evaluation, and our [eval : env -> t -> nf] function
    is now paired with a [read_back : nf -> t] function to
    back to the usual syntax of terms.
 *)
module Strong_naive = struct
  type nf = (* normal forms *)
    | Ne of ne
    | Clos of env * var * t * var * nf
       (* arguments:
         - captured environmnt
         - bound variable
           (in the input term, scopes over the body)
         - body (in the input term)
         - fresh bound variable
           (generated during evaluation, scopes over the normal form)
         - normal form of the body *)

  and ne = (* neutral terms *)
    | Var of var
    | App of ne * nf

  (* the environmnt stores normal forms *)
  and v = nf
  and env = v open_env

  let rec eval env : t -> nf = function
    | Var x ->
      begin match Ident.find_same x env with
      | Val v -> v
      | Free x -> Ne (Var x)
      end
    | Abs (x, t) ->
      let y = fresh x in
      Clos (env, x, t, y,
       let env' = Ident.add x (Free y) env in
       eval env' t)
      (* Remark on scoping (see previous remarks).

         [eval env t] takes an environment/substitution from [Gamma]
         to [Delta], a term in environment [Gamma] and returns
         a normal form in [Delta].

         Now evaluating the body of [Abs(x,body)] returns not only
         the normal form of [body] (in (Delta, x)) but *also* the
         source form [body]. This source subterm is under (Gamma, x)
         instead, so we need to close over [env] to be able to
         interpret [body] during application later. *)
    | App (t, u) ->
      let f, arg = eval env t, eval env u in
      match f with
      | Ne n -> Ne (App (n, arg))
      | Clos (env', x, body, _y, _v) ->
        (* when an abstraction is applied, we used only
           the body as it was in the source term, not its
           normal form *)
        eval (Ident.add x (Val arg) env') body

  let rec read_back : nf -> t = function
    | Clos (env', _x, _t, y, v) -> Abs (y, read_back v)
        (* when an abstraction is return, we used only
           the normal form of the body, not its source subterm. *)
    | Ne (App (n, v)) -> App (read_back (Ne n), read_back v)
    | Ne (Var x) -> Var x

  let normalize env t =
    read_back (eval env t)
end

let test_result = test (module Strong_naive : Eval)
let () = check_result test_result

(** {3 Naive iterated-weak evaluator}

    For comparison, this is a (naive) strong evaluator based on
    Grégoire and Leroy's "iterated weak reduction". Now the
    normalization under abstractions is deferred in [read_back], so
    the code of [eval] and the type of normal forms arech closer to
    a weak strategy, but (in absence of specific sharing) this may
    duplicate the evaluation of abstraction bodies. *)
module Strong_by_value_iterated = struct
  type nf = (* normal forms *)
    | Ne of ne
    | Clos of env * var * t (* the usual closures of weak strategies *)
  and ne = (* neutral terms *)
    | Var of var
    | App of ne * nf

  (* the Environmnt stores normal forms. *)
  and v = nf
  and env = v open_env

  let rec eval env : t -> nf = function
    | Var x ->
      begin match Ident.find_same x env with
      | Free x -> Ne (Var x)
      | Val v -> v
      end
    | Abs (x, t) ->
      (* Here we use the usual weak-call-by-value approach. *)
      Clos (env, x, t)
    | App (t, u) ->
      let f, arg = eval env t, eval env u in
      match f with
      | Ne n -> Ne (App (n, arg))
      | Clos (env', x, body) ->
        eval (Ident.add x (Val arg) env') body

  let rec read_back : nf -> t = function
    | Clos (env', x, t) ->
      let y = fresh x in
      (* ... and here we finally normalize inside function bodies.

         Note that this re-normalization may be duplicated if the same
         closure is passed to [read_back] in several places (it was
         duplicated in the normal form), making this strategy
         unreasonable.
      *)
      let env' = Ident.add x (Free y) env' in
      Abs (y, normalize env' t)
    | Ne (App (n, v)) -> App (read_back (Ne n), read_back v)
    | Ne (Var x) -> Var x

  and normalize env t =
    read_back (eval env t)
end

let _ = test (module Strong_by_value_iterated : Eval)

(** {3 Memoization utilities} *)

(** A memoizing fixpoint combinator for arity-2 functions.

     Note: we use structural hashing and comparisons for memoization,
     so this may traverse the whole input and be substantially less
     efficient than other strategies, for example those based on
     hash-consing or explicit indexing of the input terms. In practice
     this inefficient approach has been more than enough for all
     shapes encountered by the evaluator inside the OCaml compiler, so
     we did not explore further sophistication in this direction.
 *)
let memo_fix_2 (type a b c) (f : (a -> b -> c) -> a -> b -> c) : a -> b -> c =
  let table = Hashtbl.create 42 in
  let rec fix x y =
    let key = (x, y) in
    try Hashtbl.find table key
    with Not_found ->
      let result = f fix x y in
      Hashtbl.add table key result;
      result
  in fix

let memo_fix_1 (type a b) (f : (a -> b) -> a -> b) : a -> b =
  fun x ->
    memo_fix_2
      (fun mf () x -> f (mf ()) x)
      () x

(** {3 Strong naive, memoized}

   The strong naive evaluator, with memoization.
   This is a trivial, two-line change over the previous version.

   Note: it is very important to also memoize [read_back], otherwise
   all the implicit, in-memory sharing produced by [eval] is lost at
   this point and the size of normal forms blow up again.

   When both [eval] and [read_back] are memoized, the resulting
   normalized terms contain implicit sharing -- subterms of the normal
   form coming from identical inputs in identical environments are
   shared in memory. In the use-case of the OCaml compiler, we get
   shape terms of small size thanks to implicit sharing; the compiler
   then serializes these shapes in [.cmt] object files, using the
   default OCaml marshaller that (crucially) preserves sharing.

   Note: [eval env t] in this version is memoized using the pair [env,
   t]. This shares all reductions of the same subterm in the same
   environment. But sometimes you can have dynamic environments that
   differ only in some variables that are not used in [t], and then
   you would prefer to avoid recomputation. Our simple code does not
   try to do this. We could do it by trimming the environment at each
   step, which could be done efficiently by having explicit weakening
   term constructors in our input term, produced by
   pre-processing. Again, we did not find any need for this
   sophistication for the use-case of the compiler.
*)
module Strong_naive_memo = struct

  type nf = (* normal forms *)
    | Ne of ne
    | Clos of env * var * t * var * nf
  and ne = (* neutral terms *)
    | Var of var
    | App of ne * nf

  and v = nf
  and env = v open_env

  let eval = memo_fix_2 @@ fun eval (env : env) (t : t) : nf ->
    match t with
    | Var x ->
      begin match Ident.find_same x env with
      | Val v -> v
      | Free x -> Ne (Var x)
      end
    | Abs (x, t) ->
      let y = fresh x in
      Clos (env, x, t, y,
       let env' = Ident.add x (Free y) env in
       eval env' t)
    | App (t, u) ->
      let f, arg = eval env t, eval env u in
      match f with
      | Ne n -> Ne (App (n, arg))
      | Clos (env', x, body, _y, _v) ->
        eval (Ident.add x (Val arg) env') body

  let read_back = memo_fix_1 @@ fun read_back (v : nf) : t ->
    match v with
    | Clos (env', x, t, y, v) -> Abs (y, read_back v)
    | Ne (App (n, v)) -> App (read_back (Ne n), read_back v)
    | Ne (Var x) -> Var x

  let normalize env t =
    read_back (eval env t)
end

let test_result = test (module Strong_naive_memo : Eval)
let () = check_result test_result

(** This Thunk module exposes a subset of the interface of the Lazy
   module provided by OCaml, with a different representation that uses
   object types internally. The reason for this twist is technical:
   OCaml refuses to compare or hash function closures, but it will
   compare or hash objects by identity (each object carries
   a unique integer). Using Lazy.t directly plays very badly with
   memoization (it crashes), the object wrapper works around this
   subtlety. *)
module Thunk = struct
  type 'a t = < force : 'a >
  let from_fun (type a) (f : unit -> a) : a t =
    let th = Lazy.from_fun f in
    object method force = Lazy.force th end
  let force (type a) (thunk : 'a t) : 'a =
    thunk#force
end

module Strong_by_need_memo_thunks = struct
  type nf = (* normal forms *)
    | Ne of ne
    | Clos of env * var * t * var * dnf
  and dnf = nf Thunk.t (* delayed/thunked normal forms *)
  and ne = (* neutral terms *)
    | Var of var
    | App of ne * dnf

  and v = dnf and env = v open_env

  let eval = memo_fix_2 @@ fun eval env (t : t) ->
    let delay_eval env v = Thunk.from_fun (fun () -> eval env v) in
    match t with
    | Var x ->
      begin match Ident.find_same x env with
      | Free x -> Ne (Var x)
      | Val dv -> Thunk.force dv
      end
    | Abs (x, t) ->
      let y = fresh x in
      Clos (env, x, t, y, delay_eval (Ident.add x (Free y) env) t)
    | App (t, u) ->
      let f, arg = eval env t, delay_eval env u in
      match f with
      | Ne n -> Ne (App (n, arg))
      | Clos (env', x, body, _y, _v) ->
        eval (Ident.add x (Val arg) env') body

  let read_back = memo_fix_1 @@ fun read_back (v : nf) : t ->
    let read_back_force dv = read_back (Thunk.force dv) in
    match v with
    | Clos (_env', _x, _t, y, dv) ->
      Abs (y, read_back_force dv)
    | Ne (App (n, dv)) -> App (read_back (Ne n), read_back_force dv)
    | Ne (Var x) -> Var x

  let normalize env t =
    read_back (eval env t)
end

let test_result = test (module Strong_by_need_memo_thunks : Eval)
let () = check_result test_result


(** The same idea, without the explicit thunks.

   The reason why we needed to remove thunks in our implementation is
   that our Thunk module uses objects internally, and the OCaml
   compiler has decided to not use objects to make bootstrapping
   simpler. (We don't want bugs in the object layer, which is more
   complex, to affect the correctness of the compiler itself.)

   There would be other ways to work around this restriction to not
   use objects, for example by generating unique identifiers for
   thunks ourselves and then implementing our own comparison/hashing
   functions. This would also work but it is subtantially more work
   than the object-wrapping workaround or this new version.
 *)
module Strong_by_need_memo_nothunks = struct
  type nf = (* normal forms *)
    | Ne of ne
    | Clos of env * var * t * var * dnf
  and dnf = Delayed of env * t (* delayed normal forms *)
  and ne = (* neutral terms *)
    | Var of var
    | App of ne * dnf

  and v = dnf and env = v open_env

  let force eval (Delayed (env, t)) = eval env t

  let eval = memo_fix_2 @@ fun eval env (t : t) : nf ->
    let delay_eval env v = Delayed (env, v) in
    match t with
    | Var x ->
      begin match Ident.find_same x env with
        | Free x -> Ne (Var x)
        | Val dv -> force eval dv
      end
    | Abs (x, t) ->
      let y = fresh x in
      Clos (env,
            x, t,
            y, delay_eval (Ident.add x (Free y) env) t)
    | App (t, u) ->
      let f, arg = eval env t, delay_eval env u in
      match f with
      | Ne n -> Ne (App (n, arg))
      | Clos (env', x, body, _y, _v) ->
        eval (Ident.add x (Val arg) env') body

  let read_back = memo_fix_1 @@ fun read_back (v : nf) : t ->
    let read_back_force dv = read_back (force eval dv) in
    match v with
    | Clos (env', x, t, y, dv) -> Abs (y, read_back_force dv)
    | Ne (App (n, v)) -> App (read_back (Ne n), read_back_force v)
    | Ne (Var x) -> Var x

  let normalize env t =
    read_back (eval env t)
end

let test_result = test (module Strong_by_need_memo_nothunks : Eval)
let () = check_result test_result


(** In fact the _nothunks approach can be reformulated
    as a by-need cousin of the _iterated style. *)
module Strong_by_need_memo_iterated = struct
  type nf = (* normal forms *)
    | Ne of ne
    | Clos of env * var * t
  and dnf = Delayed of env * t
  and ne = (* neutral terms *)
    | Var of var
    | App of ne * env * t

  and v = dnf and env = v open_env

  let force eval (Delayed (env, t)) = eval env t

  let eval = memo_fix_2 @@ fun eval env (t : t) : nf ->
    match t with
    | Var x ->
      begin match Ident.find_same x env with
      | Free x -> Ne (Var x)
      | Val dv -> force eval dv
      end
    | Abs (x, t) ->
      Clos (env, x, t)
    | App (t, u) ->
      match eval env t with
      | Ne n -> Ne (App (n, env, u))
      | Clos (env', x, body) ->
        eval (Ident.add x (Val (Delayed (env, u))) env') body

  let read_back = memo_fix_1 @@ fun read_back (v : nf) : t ->
    let normalize env t = read_back (eval env t) in
    match v with
    | Clos (env', x, t) ->
      let y = fresh x in
      let env' = Ident.add x (Free y) env' in
      Abs (x, normalize env' t)
    | Ne (App (n, env', v)) ->
      App (read_back (Ne n), normalize env' v)
    | Ne (Var x) -> Var x

  let normalize env t =
    read_back (eval env t)
end

let test_result = test (module Strong_by_need_memo_iterated : Eval)
let () = check_result test_result
