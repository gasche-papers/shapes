\RequirePackage{boolean}

\newcommand{\Print}{\False}
\newcommand{\Beamer}{\True}

\IfFileExists
    {\jobname.cfg}
    {\input {\jobname.cfg}\def\OPTIONS{}}
    {}

\documentclass[dvipsnames,svgnames]{beamer}
%\usetheme{Madrid} % My favorite!
\usetheme{Boadilla} % Pretty neat, soft color.
%\usetheme{default}
%\usetheme{Warsaw}
%\usetheme{Bergen} % This template has nagivation on the left
% \usetheme{Frankfurt} % Similar to the default
%with an extra region at the top.
%\usecolortheme{seahorse} % Simple and clean template
%\usetheme{Darmstadt} % not so good
% Uncomment the following line if you want %
% page numbers and using Warsaw theme%
% \setbeamertemplate{footline}[page number]
%\setbeamercovered{transparent}
\setbeamercovered{invisible}
% To remove the navigation symbols from
% the bottom of slides%
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}{\hfill\insertframenumber\hfill\vspace{3mm}}

\usepackage{mathpartir}
\usepackage{natbib}
\usepackage{tikz-cd}

\hypersetup{colorlinks=true,citecolor=OliveGreen,%
  pdftitle={Reducing shapes},%
  pdfauthor={Nathanaelle Courant, Gabriel Scherer},
}

% to avoid confusion with \ob{} color (blue), use orange-like titles
\setbeamercolor*{titlelike}{fg=Orange!80!Purple}

\title{Reducing shapes}
\author{\emph{Gabriel Scherer}, Nathana{\"e}lle Courant}

% \institute[INRIA]{Parsifal, INRIA Saclay\\
%   \href{https://github.com/ocaml/ocaml/}{OCaml}}
% \date{\today}

\usepackage{lib}
\usepackage{beamer_lib}

\begin{document}
%

\begin{frame}
  \titlepage

\begin{center}
  \includegraphics[height=3cm]{pictures/Gabriel_Scherer.jpg}
  \hspace{2em}
  \includegraphics[height=3cm]{pictures/Nathanaelle_Courant.jpg}
\end{center}
\end{frame}

\begin{frame}{A metaphor}
  \begin{center}
    \includegraphics[height=5.5cm]{squid_on_a_beach.jpg}

    G.W. Tryon Jr. (1879)

    \small{\url{https://commons.wikimedia.org/wiki/File:Ommastrephes_mouchezi.jpg}}

    The giant squid that washed ashore on Île Saint-Paul on 2 November
    1874.
  \end{center}
\end{frame}

\begin{frame}
  In case you wonder where {\^I}le Saint-Paul is:\pause

  \begin{center}
    \includegraphics[height=8cm]{ile-saint-paul.png}
  \end{center}
\end{frame}

\begin{frame}{This talk}
  A field report on the

  \begin{center}first sighting (to our knowledge)\end{center}

  of \pause

  \begin{center}strong (by-need) reduction\end{center}

  in the wild, outside proof assistants.

  \vfill

  Note: I'm not an expert!
\end{frame}

\begin{frame}{Shapes}
  Shapes, as designed by Thomas Refis, Ulysse G{\'e}rard and Leo White,

  are $\lambda$-terms representing the shape of OCaml modules -- and source files.

  (no term-level information except source locations)

  \vfill

  They extended the OCaml compiler to compute shapes

  and store them in object files.

  \vfill

  Motivation: tooling support: ``where is \texttt{foo} defined?''

  (requires normalization)
\end{frame}

\begin{frame}[fragile]{Shape computations}
  \[\begin{tikzcd}
      \text{foo.ml}
      \arrow{d}{\text{ocamlc}}
      &
      &
      \text{bar.ml}
      \arrow{d}{\text{ocamlc}}
      \\
      \text{foo.cmt}
      \arrow{dr}
      & &
      \text{bar.cmt}
      \arrow{dl}
      \\
      &
      \text{whole project (merlin)}
      &
      \end{tikzcd}
    \]


  \vfill

  Separate compilation: the shape of a module is an \emph{open term}.

  \vfill

  Definition lookup inside functors: we want \emph{strong reduction}.
\end{frame}

\begin{frame}{Problem}
  A naive implementation of strong reduction does fine in general, but it \pause

  \begin{center}
    explodes
  \end{center}

  on some complex functor-using OCaml programs. (Irmin)
\end{frame}

\begin{frame}{Solution}
  Ulysse G{\'e}rard and Thomas Refis implemented some optimizations;

  enough for ``termination'' but still unsatisfying.

  \vfill

  Strong call-by-need reduction avoids blowups.

  \vfill

  Performance on a problematic source file:
  \begin{tabular}{l|r|r|}
                          & compilation time & output size \\ \hline
    no shapes             & 0.39s & 2538Kio : 2.5Mio \\
    shapes, naive +opts   & 2.15s & 91Mio \\
    shapes, strong cbneed & 0.40s & 2552Kio : 2.5Mio \\
  \end{tabular}
\end{frame}

\begin{frame}[fragile]{Why the blowup?}
  Consider:
  \begin{lstlisting}
  module M = struct
    let x = A.x
    let y = A.y
    let z = A.z
  end
  \end{lstlisting}

  With closed reduction, this only reduces when \texttt{A} is a structure/record.
  \newcommand{\norm}[1]{\|#1\|}
  \[ \norm{M} \leq \norm{A} \]

  With open reduction, \texttt{A} may be neutral: \texttt{F(X).Bar}. Then:
  \[ \norm{M} \simeq 3 * \norm{A} \]

  \vfill
  
  Actually a very common pattern:

  \begin{lstlisting}
    module M = (A : S)
  \end{lstlisting}

\end{frame}

\begin{frame}{Why the blowup? Intuition}
  Intuition:

  closed, weak reduction has size-exploding examples,

  but strong reduction explodes \emph{more}

  \vfill

  More precisely:

  some realistic closed programs have small normal forms,

  but their subterms could blow up under strong reduction.
\end{frame}

\begin{frame}
  \begin{center}
    Thanks!
  \end{center}

  \vfill

  (Bonus slides follow.)
\end{frame}

\begin{frame}[fragile]{A terrible implementation}
  \begin{lstlisting}
  let rec eval env : t -> t = function
    | Var x ->
      Ident.find_same x env
    | Abs (x, t) ->
      Abs (x,
       let env' = Ident.add x (Var x) env in
       eval env' t)
    | App (t, u) ->
      let f, arg = eval env t, eval env u in
      match f with
      | (Var _ | App _) as ne -> App (ne, arg)
      | Abs (x, body) ->
        eval (Ident.add x arg env) body
  \end{lstlisting}
\end{frame}


\begin{frame}[fragile]{A naive implementation (1): types}
  \begin{lstlisting}
  type nf = (* normal forms *)
    | Ne of ne
    | Clos of env * var * t * var * nf
  and ne = (* neutral terms *)
    | Var of var
    | App of ne * nf

  type open_value =
    | Val of nf
    | Free of var
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{A naive implementation (2): code}
  \begin{lstlisting}
  let rec eval = fun env (t : t) : nf ->
    match t with
    | Var x -> begin match Ident.find_same x env with
      | Val v -> v
      | Free x -> Ne (Var x)
      end
    | Abs (x, t) ->
      let y = fresh x in
      Clos (env, x, t, y,
       let env' = Ident.add x (Free y) env in
       eval env' t)
    | App (t, u) ->
      let f, arg = eval env t, eval env u in
      match f with
      | Ne n -> Ne (App (n, arg))
      | Clos (env', x, body, _y, _v) ->
        eval (Ident.add x (Val arg) env') body
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{A naive implementation (3): memoization}
  \begin{lstlisting}
  let eval = memo_fix_2 @@ fun eval env (t : t) : nf ->
    match t with
    | Var x -> begin match Ident.find_same x env with
      | Val v -> v
      | Free x -> Ne (Var x)
      end
    | Abs (x, t) ->
      let y = fresh x in
      Clos (env, x, t, y,
       let env' = Ident.add x (Free y) env in
       eval env' t)
    | App (t, u) ->
      let f, arg = eval env t, eval env u in
      match f with
      | Ne n -> Ne (App (n, arg))
      | Clos (env', x, body, _y, _v) ->
        eval (Ident.add x (Val arg) env') body
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{A by-need implementation (1)}
  \begin{lstlisting}
  type nf = (* normal forms *)
    | Ne of ne
    | Clos of env * var * t * var * dnf
  and dnf = nf Lazy.t
  and ne = (* neutral terms *)
    | Var of var
    | App of ne * dnf

    let force eval env t = lazy (eval env)
    let delay eval env dv = Lazy.force dv
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{A naive implementation: reminder}
  \begin{lstlisting}
  let eval = memo_fix_2 @@ fun eval env (t : t) : nf ->
    match t with
    | Var x -> begin match Ident.find_same x env with
      | Val v -> /*v*/
      | Free x -> Ne (Var x)
      end
    | Abs (x, t) ->
      let y = fresh x in
      Clos (env, x, t, y,
        let env' = Ident.add x (Free y) env in
        /*eval env' t*/)
    | App (t, u) ->
      let f, arg = eval env t, /*eval env u*/ in
      match f with
      | Ne n -> Ne (App (n, arg))
      | Clos (env', x, body, _y, _v) ->
        eval (Ident.add x (Val arg) env') body
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{A by-need implementation (2)}
  \begin{lstlisting}
  let eval = memo_fix_2 @@ fun eval env (t : t) : nf ->
    match t with
    | Var x -> begin match Ident.find_same x env with
      | Val v -> /*force eval v*/
      | Free x -> Ne (Var x)
      end
    | Abs (x, t) ->
      let y = fresh x in
      Clos (env, x, t, y,
        let env' = Ident.add x (Free y) env in
        /*delay eval env' t*/)
    | App (t, u) ->
      let f, arg = eval env t, /*delay eval env u*/ in
      match f with
      | Ne n -> Ne (App (n, arg))
      | Clos (env', x, body, _y, _v) ->
        eval (Ident.add x (Val arg) env') body
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{A by-need implementation (3)}
  \begin{lstlisting}
  type nf = (* normal forms *)
    | Ne of ne
    | Clos of env * var * t * var * dnf
  and dnf = Delayed of env * t
  and ne = (* neutral terms *)
    | Var of var
    | App of ne * dnf

    let force eval (env, t) = eval env t
    let delay eval env t = (env, t)
  \end{lstlisting}

  \pause\vfill

  If you squint: a by-need version
  of iterated weak reduction.
\end{frame}
\end{document}
