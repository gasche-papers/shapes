LATEXMK=latexmk -pdf -bibtex

.PHONY: all
all: workshop.pdf

workshop.pdf: workshop.tex shapes.bib
	$(LATEXMK) workshop.tex

workshop.tex: workshop.md
	pandoc workshop.md --standalone -o workshop.tex

.PHONY: clean
clean:
	$(LATEXMK) -c
	rm workshop.tex
